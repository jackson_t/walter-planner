const { app, BrowserWindow } = require("deskgap");

app.once("ready", () => {
  const window = new BrowserWindow({
    title: "Walter",
    width: 1220,
    height: 740,
    center: true
  });

  window.loadFile("../../web/cosmic-conure/src/index.html");

  window.setTitle("Walter");
  window.setMinimumSize(960, 540);
});

