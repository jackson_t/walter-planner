function main() {
  // if (!window.chrome)
  //   alert("Please use a Chromium-based browser for better rendering.");

  let input = document.querySelector(".src textarea");

  input.innerHTML = jsyaml.load(`
  input: |
    ##############################################
    # Attack Tree Generator by @Jackson_T
    # https://gitlab.com/jthuraisamy/attack-tree
    #
    # This YAML template can be used to document
    # attack paths for penetration tests and red
    # team operations.
    #
    # - Human-readable: use your favourite text
    #   editor, and version control with a team.
    #
    # - Machine-readable: render the data, build
    #   front-ends, and allow automated analysis.
    #
    # Node types:
    #
    # - Artifacts: items that can further
    #   operation goals (e.g. discovered
    #   credentials, hosts, documentation).
    #
    # - Capabilities: desirable abilities that
    #   artifacts contribute to (e.g. exploits,
    #   production or administrative access).
    #
    # - Impacts: business or technical impacts
    #   the operation seeks to achieve with
    #   used artifacts and capabilities.
    #
    # You can describe each node with tags
    # and create links to form directional
    # relationships. Links can be re-ordered
    # to adjust node positioning. Tags are
    # customizable and can optionally use
    # Font Awesome icons.
    #
    # The example below references disclosed
    # attack paths in the Oracle OPERA hotel
    # management software to access sensitive
    # cardholder and reservation data. Details
    # here: http://jackson-t.ca/oracle-opera.html
    ##############################################

    Artifacts:
    - slug: shodan-hosts
      name: Hosts found on Shodan # EDIT ME!
      tags:
        status: Complete
        zone: Internet
        authn: False
        sophistication: Novice
      details: >
        Example: https://www.shodan.io/search?query=Micros+Opera

    - slug: war-file
      name: Application backend<br>bytecode found online
      tags:
        status: Complete
        zone: Internet
        authn: False
        sophistication: Novice
      details: >
        Download: http://1.186.80.26/MICROS/opera/operaias/JSERV/J2EEAPPS/webarchive.war

        
    Capabilities:
    - slug: session-hijack-vuln
      name: Session hijack exploit
      tags:
        status: Complete
        zone: Internet
        authn: False
        sophistication: Practitioner
      details: CVE-2016-5565

    - slug: rce-vuln
      name: App server RCE exploit
      tags:
        status: Complete
        zone: Internet
        authn: False
        sophistication: Practitioner
      details: CVE-2016-5563

    - slug: sql-exposure-vuln
      name: Database connection<br>string exposure
      tags:
        status: Complete
        zone: Internet
        authn: False
        sophistication: Practitioner
      details: >
        Host name is in "CRS Servlet Status
        Information" page and credentials are
        in the response to LaunchOperaApp.do.
        Default creds are opera/opera.

    - slug: admin-app-access
      name: Administrative application access
      tags:
        status: Complete
        zone: Internet
        authn: True
        sophistication: Practitioner

    - slug: admin-db-access
      name: Administrative database access
      tags:
        status: Complete
        zone: Intranet
        authn: True
        sophistication: Practitioner

    - slug: decrypt-cc
      name: Decrypt credit card data
      tags:
        status: Pending
        zone: Intranet
        authn: True
        sophistication: Practitioner
      

    Impacts:
    - slug: cardholder-data
      name: Cardholder data exposure

    - slug: reservations
      name: Read and tamper reservations


    Links:
    - source: war-file
      destination: session-hijack-vuln
      name: Known path to logs with<br>active session tokens

    - source: war-file
      destination: rce-vuln
      name: Chained unauthenticated<br>RFI and command injection

    - source: war-file
      destination: sql-exposure-vuln
      name: Unauthenticated paths<br>to SQL connection string

    - source: shodan-hosts
      destination: admin-app-access

    - source: session-hijack-vuln
      destination: admin-app-access
      name: Wait for admin to login<br>and use their token

    - source: rce-vuln
      destination: admin-db-access
      name: Connect to database<br>from application server

    - source: sql-exposure-vuln
      destination: admin-db-access

    - source: admin-app-access
      destination: reservations

    - source: admin-db-access
      destination: decrypt-cc
      name: Using 3DES keys and data found in DB

    - source: admin-db-access
      destination: reservations

    - source: decrypt-cc
      destination: cardholder-data


    Tags:
    - slug: zone
      name: Zone
      opts:
      - name: Physical
        icon: fa-hand-paper
      - name: Social
        icon: fa-user-friends
      - name: Intranet
        icon: fa-building
      - name: Internet
        icon: fa-globe

    - slug: sophistication
      name: Sophistication Level
      opts:
      - name: Aspirant
      - name: Novice
      - name: Practitioner
      - name: Expert
      - name: Innovator

    - slug: actor
      name: Threat Actor
      opts:
      - name: Mischievous Enthusiast
      - name: Script Kiddies
      - name: Hacktivist
      - name: Organized Crime
      - name: Nation State

    - slug: authn
      name: Authentication Required
      opts:
      - name: True
        icon: fa-lock
      - name: False
        icon: fa-lock-open

    - slug: status
      name: Status
      opts:
      - name: Complete
        icon: fa-check-square
      - name: Pending
        icon: fa-square
      - name: Missed
        icon: fa-times
  `).input;

  var editor = CodeMirror.fromTextArea(input, {
    mode: "yaml",
    theme: "idea",
    tabSize: 2
  });

  mermaid.initialize({ theme: "neutral" });
  editor.on("change", e => { render(editor); });
  render(editor);
}

function render(editor) {
  try {
    let AttackTree = jsyaml.load(editor.getValue());

    function getHeaderMarkupForNode(type, node) {
      if (node.tags.status == "Complete")
        return `<sup>${type}</sup>`;
      else
        return `<sup><i>${node.tags.status} ${type}</i></sup>`;
    }

    function getFooterMarkupForNode(node) {
      let tags = [];
      for (let tagSlug of Object.keys(node.tags))
        if (tag = AttackTree.Tags.filter(t => { return t.slug == tagSlug; })[0])
          if (tagOption = tag.opts.filter(o => { return o.name == node.tags[tagSlug]; })[0])
            tags.push((tagOption.icon) ? `fa:${tagOption.icon}` : tagOption.name);
      return `<sub>${tags.join(" / ")}</sub>`;
    }

    let artifacts = AttackTree.Artifacts.map(a => {
      return `${a.slug}["${getHeaderMarkupForNode("Artifact", a)}<br><b>${a.name}</b><br>${getFooterMarkupForNode(a)}"]`;
    });
    
    let capabilities = AttackTree.Capabilities.map(c => {
      return `${c.slug}("${getHeaderMarkupForNode("Capability", c)}<br><b>${c.name}</b><br>${getFooterMarkupForNode(c)}")`;
    });
    
    let impacts = AttackTree.Impacts.map(i => {
      return `${i.slug}>"<sup>Impact</sup><br><b>${i.name}</b>"]`;
    });
    
    let links = AttackTree.Links.map(l => {
      let nodes = AttackTree.Artifacts.concat(AttackTree.Capabilities);
      let node = nodes.filter(n => { return n.slug == l.source; })[0];

      if (node.tags.status == "Complete")
        return `${l.source} -->|${(l.name) ? l.name : " "}| ${l.destination}`;
      else
        return `${l.source}-${(l.name) ? "." + l.name + " " : ""}.-> ${l.destination}`;
    });

    let mermaidInput = `
      graph TD

      %% Artifacts
      ${artifacts.join("\n    ")}

      %% Capabilities
      ${capabilities.join("\n    ")}

      %% Impacts
      ${impacts.join("\n    ")}

      %% Links
      ${links.join("\n    ")}

      %% Defined classes
      classDef artifact fill:#a6d6e7,stroke:#0c1c54,stroke-width:2px;
      classDef capability fill:#cee49a,stroke:#13540c,stroke-width:2px;
      classDef impact fill:gold,stroke:darkgoldenrod,stroke-width:2px;

      %% Applied classes
      class ${AttackTree.Artifacts.map(i => { return i.slug; }).join(",")} artifact;
      class ${AttackTree.Capabilities.map(i => { return i.slug; }).join(",")} capability;
      class ${AttackTree.Impacts.map(i => { return i.slug; }).join(",")} impact;
    `;

    // Remove all children from destination div.
    let dst = document.querySelector(".dst");
    while (dst.hasChildNodes())
      dst.removeChild(dst.firstChild);
    
    // Render new markup with Mermaid.
    mermaid.render("dst", mermaidInput, (svgCode) => { dst.innerHTML = svgCode });

    // Fix edge labels "text-cutoff" issue.
    document.querySelectorAll('g.edgeLabel g foreignObject').forEach(o => {
      o.width.baseVal.value += 50;
      o.height.baseVal.value += 25;
    });
  } catch(error) {
    console.error(error);

    // Remove all children from destination div.
    let dst = document.querySelector(".dst");
    while (dst.hasChildNodes())
      dst.removeChild(dst.firstChild);

    let child = document.createElement('pre');
    child.className = 'error-message'
    child.innerText = (!!window.chrome) ? error.stack : `${error.name}: ${error.message}\n\n${error.stack}`;
    
    document.querySelector(".dst").appendChild(child);
  }
}

if (window.addEventListener) window.addEventListener("load", main, false);
else if (window.attachEvent) window.attachEvent("onload", main);
else window.onload = main;