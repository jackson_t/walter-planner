const GlobalState = {
  Components: {
    cmEditor: null,
    cmEditorTimeout: null
  },

  Preferences: {
    isFirstVisit: true,
    isTreeFlipped: false,
    isNightModeOn: false
  },

  Streams: {
    selectedStream: null,
    streams: []
  },

  Templates: {
    basic: jsyaml.load(`
      input: |
        ###############################################
        # Walter: Attack Path Planner
        #
        # Walter can be used to document and visualize
        # attack paths for penetration tests and red
        # team operations.
        #
        # Node types:
        #
        # - Artifacts: items that can further
        #   operation goals (e.g. discovered hosts,
        #   credentials, code, documentation).
        #
        # - Capabilities: desirable abilities that
        #   artifacts contribute to (e.g. exploits,
        #   production or administrative access).
        #
        # - Impacts: business or technical impacts
        #   the operation seeks to achieve with
        #   defined capabilities and artifacts.
        #
        # You can describe each node with tags and
        # create links to form directional relation-
        # ships. Links can be re-ordered to adjust
        # node positioning. Tags are customizable
        # and can optionally use Font Awesome icons.
        ###############################################

        Artifacts:
        - slug: leaked-creds
          name: Leaked admin credentials
          tags:
            status: Complete
            zone: Internet

            
        Capabilities:
        - slug: admin-app-access
          name: Administrative application access
          tags:
            status: Complete
            zone: Internet


        Impacts:
        - slug: customer-data
          name: Read and tamper customer data


        Links:
        - src: leaked-creds
          dst: admin-app-access

        - src: admin-app-access
          dst: customer-data


        Tags:
        - slug: status
          name: Status
          opts:
          - name: Complete
            icon: fa-check-square
          - name: Pending
            icon: fa-square
          - name: Missed
            icon: fa-times

        - slug: zone
          name: Zone
          opts:
          - name: Physical
            icon: fa-hand-paper
          - name: Social
            icon: fa-user-friends
          - name: Intranet
            icon: fa-building
          - name: Internet
            icon: fa-globe
    `).input,

    opera: jsyaml.load(`
      input: |
        ###############################################
        # Walter: Attack Path Planner
        #
        # Walter can be used to document and visualize
        # attack paths for penetration tests and red
        # team operations.
        #
        # - Human-readable: use your favourite text
        #   editor, and version control with a team.
        #
        # - Machine-readable: render the data, build
        #   front-ends, and allow automated analysis.
        #
        # Node types:
        #
        # - Artifacts: items that can further
        #   operation goals (e.g. discovered hosts,
        #   credentials, code, documentation).
        #
        # - Capabilities: desirable abilities that
        #   artifacts contribute to (e.g. exploits,
        #   production or administrative access).
        #
        # - Impacts: business or technical impacts
        #   the operation seeks to achieve with
        #   defined capabilities and artifacts.
        #
        # You can describe each node with tags and
        # create links to form directional relation-
        # ships. Links can be re-ordered to adjust
        # node positioning. Tags are customizable
        # and can optionally use Font Awesome icons.
        #
        # The example below references disclosed
        # attack paths in the Oracle OPERA hotel
        # management software to access sensitive
        # cardholder and reservation data. Details
        # here: http://jackson-t.ca/oracle-opera.html
        ###############################################
        
        Artifacts:
        - slug: shodan-hosts
          name: Hosts found on Shodan # EDIT ME!
          tags:
            status: Complete
            zone: Internet
            authn: False
            sophistication: Novice
          details: >
            Example: https://www.shodan.io/search?query=Micros+Opera
        
        - slug: war-file
          name: Application backend<br>bytecode found online
          tags:
            status: Complete
            zone: Internet
            authn: False
            sophistication: Novice
          details: >
            Download: http://1.186.80.26/MICROS/opera/operaias/JSERV/J2EEAPPS/webarchive.war
        
            
        Capabilities:
        - slug: session-hijack-vuln
          name: Session hijack exploit
          tags:
            status: Complete
            zone: Internet
            authn: False
            sophistication: Practitioner
          details: CVE-2016-5565
        
        - slug: rce-vuln
          name: App server RCE exploit
          tags:
            status: Complete
            zone: Internet
            authn: False
            sophistication: Practitioner
          details: CVE-2016-5563
        
        - slug: sql-exposure-vuln
          name: Database connection<br>string exposure
          tags:
            status: Complete
            zone: Internet
            authn: False
            sophistication: Practitioner
          details: >
            Host name is in "CRS Servlet Status
            Information" page and credentials are
            in the response to LaunchOperaApp.do.
            Default creds are opera/opera.
        
        - slug: admin-app-access
          name: Administrative application access
          tags:
            status: Complete
            zone: Internet
            authn: True
            sophistication: Practitioner
        
        - slug: admin-db-access
          name: Administrative database access
          tags:
            status: Complete
            zone: Intranet
            authn: True
            sophistication: Practitioner
        
        - slug: decrypt-cc
          name: Decrypt credit card data
          tags:
            status: Pending
            zone: Intranet
            authn: True
            sophistication: Practitioner
          
        
        Impacts:
        - slug: cardholder-data
          name: Cardholder data exposure
        
        - slug: reservations
          name: Read and tamper reservations
        
        
        Links:
        - src: war-file
          dst: session-hijack-vuln
          lbl: Known path to logs with<br>active session tokens
        
        - src: war-file
          dst: rce-vuln
          lbl: Chained unauthenticated<br>RFI and command injection
        
        - src: war-file
          dst: sql-exposure-vuln
          lbl: Unauthenticated paths<br>to SQL connection string
        
        - src: shodan-hosts
          dst: admin-app-access
          lbl: Select a target host
        
        - src: session-hijack-vuln
          dst: admin-app-access
          lbl: Wait for admin to login<br>and use their token
        
        - src: rce-vuln
          dst: admin-db-access
          lbl: Connect to database<br>from application server
        
        - src: sql-exposure-vuln
          dst: admin-db-access
          lbl: Use exposed credentials
        
        - src: admin-app-access
          dst: reservations
          lbl: Browse to reservations panel
        
        - src: admin-db-access
          dst: decrypt-cc
          lbl: Using 3DES keys and data found in DB
        
        - src: admin-db-access
          dst: reservations
          lbl: Query reservation tables
        
        - src: decrypt-cc
          dst: cardholder-data

        
        Streams:
        - slug: app-access
          name: Application access
          nodes:
          - shodan-hosts
          - war-file
          - session-hijack-vuln
          - rce-vuln
          - sql-exposure-vuln
          - admin-app-access
          - reservations

        - slug: db-access
          name: Database access
          nodes:
          - war-file
          - rce-vuln
          - sql-exposure-vuln
          - admin-db-access
          - decrypt-cc
          - reservations
          - cardholder-data


        Tags:
        - slug: zone
          name: Zone
          opts:
          - name: Physical
            icon: fa-hand-paper
          - name: Social
            icon: fa-user-friends
          - name: Intranet
            icon: fa-building
          - name: Internet
            icon: fa-globe
        
        - slug: sophistication
          name: Sophistication Level
          opts:
          - name: Aspirant
          - name: Novice
          - name: Practitioner
          - name: Expert
          - name: Innovator
        
        - slug: actor
          name: Threat Actor
          opts:
          - name: Mischievous Enthusiast
          - name: Script Kiddies
          - name: Hacktivist
          - name: Organized Crime
          - name: Nation State
        
        - slug: authn
          name: Authentication Required
          opts:
          - name: True
            icon: fa-lock
          - name: False
            icon: fa-lock-open
        
        - slug: status
          name: Status
          opts:
          - name: Complete
            icon: fa-check-square
          - name: Pending
            icon: fa-square
          - name: Missed
            icon: fa-times      
    `).input
  }
};

class MainContainer {
  constructor(vnode) { }

  view() {
    return m("div[id='MainContainer']", [
      m(MenuComponent),
      m(WalterContainer),
      m(FooterComponent),
      m(ModalsContainer)])
  }

  oncreate(vnode) {
    if (GlobalState.Preferences.isFirstVisit)
      $("#WelcomeModalComponent.ui.modal").modal("show");
  }
}

class MenuComponent {
  constructor(vnode) { }

  view() {
    return m("div.ui.mini.fixed.inverted.menu[id='MenuComponent']", [
      m("a.item", {onclick: e => { WalterContainer.newPlan(); }}, [
        m("i.file.icon"),
        m("span", "New")]),
      m("a.item", {onclick: e => { document.querySelector("input#open-file").click(); }}, [
        m("i.folder.open.icon"),
        m("span", "Open"),
        m("input[id='open-file'][type='file'][accept='.yml, .yaml, text/plain']", {onchange: e => { WalterContainer.openPlan(e); }})]),
      m("div.ui.dropdown.item", [
        m("i.save.icon"),
        m("span", "Save"),
        m("i.dropdown.icon"),
        m("div.menu", [
          m("div.item", {onclick: e => { WalterContainer.savePlan("yaml"); }}, m("span", "YAML")),
          m("div.item", {onclick: e => { WalterContainer.savePlan("svg"); }}, m("span", "SVG")),
          m("div.item", {onclick: e => { WalterContainer.savePlan("png"); }}, m("span", "PNG"))])]),
      m("div.right.menu", [
        m("div.ui.dropdown.link.item", [
          m("i.code.branch.icon"),
          m("span", "Streams"),
          m("i.dropdown.icon"),
          m("div.menu", [
              m("div.item", {onclick: e => {
                GlobalState.Streams.selectedStream = null;
                TreeComponent.render(GlobalState.Components.cmEditor);
              }}, [
                m((GlobalState.Streams.selectedStream == null) ? "i.check.icon" : "i.icon"),
                m("span", "Show all")]),
              ...this.getStreamMenuItems()])]),
        m("div.ui.dropdown.link.item", [
          m("i.sliders.horizontal.icon"),
          m("span", "Options"),
          m("i.dropdown.icon"),
          m("div.menu", [
              m("div.item", {onclick: e => { TreeComponent.toggleTreeFlipped(); }}, [
                m((GlobalState.Preferences.isTreeFlipped) ? "i.check.icon" : "i.icon"),
                m("span", "Flip tree")]),
              m("div.item", {onclick: e => { WalterContainer.toggleNightMode(); }}, [
                m((GlobalState.Preferences.isNightModeOn) ? "i.check.icon" : "i.icon"),
                m("span", "Night mode")])])]),
        m("a.item", [
          m("i.question.icon"),
          m("span", "Help")]),
        m("a.item", {onclick: e => { $("#AboutModalComponent.ui.modal").modal("show"); }}, [
          m("i.info.icon"),
          m("span", "About")])]
        )
      ]
    );
  }

  oncreate(vnode) {
    $("#MenuComponent .ui.dropdown").dropdown({ action: "hide" });
  }

  getStreamMenuItems() {
    if (GlobalState.Streams.streams) {
      return GlobalState.Streams.streams.map(s => {
        return m("div.item", {onclick: e => {
          GlobalState.Streams.selectedStream = s.slug;
          TreeComponent.render(GlobalState.Components.cmEditor);
        }}, [
          m((GlobalState.Streams.selectedStream == s.slug) ? "i.check.icon" : "i.icon"),
          m("span", s.name)
        ]);
      });
    } else {
      return [];
    }
  };
}

class WalterContainer {
  constructor(vnode) {
    this.input = document.createElement("textarea");
  }

  view() {
    return m("div[id='WalterContainer']", [
      m(EditorComponent),
      m(TreeComponent)]
    );
  }

  oncreate(vnode) {
    document.querySelector("#EditorComponent").appendChild(this.input);
    this.input.innerHTML = GlobalState.Templates.opera;

    // Initialize CodeMirror.
    GlobalState.Components.cmEditor = CodeMirror.fromTextArea(this.input, {
      mode: "yaml",
      theme: (GlobalState.Preferences.isNightModeOn) ? "darcula" : "idea",
      tabSize: 2
    });

    // CodeMirror onChange event (don't update on every keystroke).
    GlobalState.Components.cmEditor.on("change", e => {
      clearTimeout(GlobalState.Components.cmEditorTimeout);
      
      GlobalState.Components.cmEditorTimeout = setTimeout(() => {
        TreeComponent.render(GlobalState.Components.cmEditor)
      }, 1000);
    });

    // Initialize mermaid and render diagram.
    mermaid.initialize({ theme: (GlobalState.Preferences.isNightModeOn) ? "dark" : "neutral" });
    TreeComponent.render(GlobalState.Components.cmEditor);

    // Initialize pane splitting.
    window.Split(["#EditorComponent", "#TreeComponent"], {
      sizes: [30, 70],
      minSize: [350, 0],
      onDragEnd: e => {
        if (document.querySelector("#EditorComponent").clientWidth >= 500)
          GlobalState.Components.cmEditor.setOption("lineNumbers", true);
        else
          GlobalState.Components.cmEditor.setOption("lineNumbers", false);
      }
    });
  }

  static newPlan() {
    GlobalState.Components.cmEditor.setValue(GlobalState.Templates.basic);
  }

  static openPlan(event) {
    const reader = new FileReader();

    reader.onload = () => {
      let buffer = reader.result.split(",", 2)[1];
      GlobalState.Components.cmEditor.setValue(window.atob(buffer));
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  static savePlan(format) {
    const ymlString = GlobalState.Components.cmEditor.getValue();
    const svgString = new XMLSerializer().serializeToString(document.querySelector("svg#mermaid-output"));
    
    switch (format) {
      case "yaml":
        window.saveAs(new File([ymlString], "walter-output.yaml", {type: "text/plain; charset=utf-8"}));      
        break;
    
      case "svg":
        window.saveAs(new File([svgString], "walter-output.svg", {type: "image/svg+xml; charset=utf-8"}));
        break;

      case "png":
        // ToDo: re-render it without node footer (tags) as a temporary fix.
        // ToDo: consider using https://github.com/gripeless/pico.
        let size = document.querySelector("svg#mermaid-output").viewBox.baseVal;
        let canvas = document.createElement("canvas");
        let ctx = canvas.getContext("2d");
        let img = document.createElement("img");

        canvas.width = size.width;
        canvas.height = size.height;
        
        img.onload = () => {
          ctx.drawImage(img, 0, 0);
          setTimeout(() => {canvas.toBlob((blob) => { saveAs(blob, "walter-output.png"); })}, 1000);
        };

        img.setAttribute("src", "data:image/svg+xml;base64," + window.btoa(svgString));
        break;

      default:
        break;
    }
  }

  static toggleNightMode() {
    // Save preference.
    let isNightModeOn = !GlobalState.Preferences.isNightModeOn;
    GlobalState.Preferences.isNightModeOn = isNightModeOn;

    GlobalState.Components.cmEditor.setOption("theme", (isNightModeOn) ? "darcula" : "idea");
    document.body.style.backgroundColor = (isNightModeOn) ? "#2b2b2b" : "white";
    document.querySelector('div.gutter').style.backgroundColor = (isNightModeOn) ? "#2a2a2a" : "#fbfbfb";
    TreeComponent.render(GlobalState.Components.cmEditor);
    m.redraw();
  }
}

class EditorComponent {
  constructor(vnode) { }

  view() {
    return m("div[id='EditorComponent']");
  }
}

class TreeComponent {
  constructor(vnode) { }

  view() {
    return m("div[id='TreeComponent'][data-deskgap-drag='']");
  }

  static render(editor) {
    try {
      // Parse attack tree from editor.
      let AttackTree = jsyaml.load(editor.getValue());

      // Validate tree for expected errors.
      // e.g. check for duplicate slugs.

      // Load streams into menu after parsing.
      GlobalState.Streams.streams = AttackTree.Streams;
      m.redraw();

      if (GlobalState.Streams.selectedStream) {
        AttackTree = TreeComponent.filterTreeByStream(AttackTree, GlobalState.Streams.selectedStream);
      }
  
      let artifacts = AttackTree.Artifacts.map(a => {
        return `${a.slug}["${TreeComponent.getHeaderMarkupForNode("Artifact", a)}<br><b>${a.name}</b><br>${TreeComponent.getFooterMarkupForNode(AttackTree, a)}"]`;
      });
      
      let capabilities = AttackTree.Capabilities.map(c => {
        return `${c.slug}("${TreeComponent.getHeaderMarkupForNode("Capability", c)}<br><b>${c.name}</b><br>${TreeComponent.getFooterMarkupForNode(AttackTree, c)}")`;
      });
      
      let impacts = AttackTree.Impacts.map(i => {
        return `${i.slug}>"<sup>Impact</sup><br><b>${i.name}</b>"]`;
      });
      
      let links = AttackTree.Links.map(l => {
        let nodes = AttackTree.Artifacts.concat(AttackTree.Capabilities);
        let node = nodes.filter(n => { return n.slug == l.src; })[0];
  
        if (!node.tags)
          return `${l.src} -->|${(l.lbl) ? l.lbl : " "}| ${l.dst}`;
        else if (node.tags.status == "Complete")
          return `${l.src} -->|${(l.lbl) ? l.lbl : " "}| ${l.dst}`;
        else
          return `${l.src}-${(l.lbl) ? "." + l.lbl + " " : ""}.-> ${l.dst}`;
      });
  
      let mermaidInput = `
        graph ${(GlobalState.Preferences.isTreeFlipped) ? "BT" : "TD"}
  
        %% Artifacts
        ${artifacts.join("\n    ")}
  
        %% Capabilities
        ${capabilities.join("\n    ")}
  
        %% Impacts
        ${impacts.join("\n    ")}
  
        %% Links
        ${links.join("\n    ")}
  
        %% Defined classes
        classDef artifact fill:#a6d6e7,stroke:#0c1c54,stroke-width:2px;
        classDef capability fill:#cee49a,stroke:#13540c,stroke-width:2px;
        classDef impact fill:gold,stroke:darkgoldenrod,stroke-width:2px;
  
        %% Applied classes
        class ${AttackTree.Artifacts.map(i => { return i.slug; }).join(",")} artifact;
        class ${AttackTree.Capabilities.map(i => { return i.slug; }).join(",")} capability;
        class ${AttackTree.Impacts.map(i => { return i.slug; }).join(",")} impact;
      `;
  
      // Remove all children from destination div.
      let dst = document.querySelector("#TreeComponent");
      while (dst.hasChildNodes())
        dst.removeChild(dst.firstChild);
      
      // Render new markup with Mermaid.
      mermaid.render("mermaid-output", mermaidInput, (svgCode) => { dst.innerHTML = svgCode });
  
      // Fix remaining rendering issues.
      document.querySelectorAll("foreignObject").forEach(o => { o.width.baseVal.value += 50; o.height.baseVal.value += 10; });
      document.querySelectorAll("g.node > g.label > g > foreignObject > div").forEach(o => { o.style.marginTop = "0.1em"; });
      document.querySelectorAll("g.impact").forEach(o => { o.transform.baseVal.getItem(0).matrix.e += 10; });
      document.querySelectorAll('svg#mermaid-output style')[1].innerHTML = `
        @import url('https://use.fontawesome.com/releases/v5.7.2/css/all.css');
        
        #mermaid-output {
          color: rgba(0, 0, 0, 0.87);
          font: normal normal 400 normal 14px / normal Lato, "Helvetica Neue", Arial, Helvetica, sans-serif;
        }`;

      // Apply night-mode styling if applicable.
      if (GlobalState.Preferences.isNightModeOn) {
        document.querySelector("svg#mermaid-output").style.backgroundColor = "#2b2b2b";
        document.querySelectorAll("g.edgePath > path").forEach(o => { o.style.stroke = "#d8d8d8" });
        document.querySelectorAll("path.arrowheadPath").forEach(o => { o.style.fill = "#d8d8d8"; });
        document.querySelectorAll("g.edgeLabel span.edgeLabel").forEach(o => { o.style.backgroundColor = "#2b2b2b"; o.style.color = "#d8d8d8"; });
      } else {
        document.querySelector("svg#mermaid-output").style.backgroundColor = "white";
      }

      // Make nodes clickable.
      document.querySelectorAll("g.node").forEach(node => {
        // Set pointer.
        node.style.cursor = "pointer";

        // Set onClick event.
        node.onclick = e => {
          let slug = node.id;
          let cursor = GlobalState.Components.cmEditor.getSearchCursor(new RegExp(`^- slug: ${slug}$`));
          
          if (cursor.findNext()) {
            let height = GlobalState.Components.cmEditor.getScrollInfo().clientHeight;
            GlobalState.Components.cmEditor.scrollIntoView(cursor.pos.from, height / 2);
            GlobalState.Components.cmEditor.setSelection(cursor.pos.from, cursor.pos.to);
          }
        };
      });
    } catch(error) {
      console.error(error);
  
      // Remove all children from destination div.
      let dst = document.querySelector("#TreeComponent");
      while (dst.hasChildNodes())
        dst.removeChild(dst.firstChild);
  
      let child = document.createElement("pre");
      child.className = "error-message";
      child.innerText = (!!window.chrome) ? error.stack : `${error.name}: ${error.message}\n\n${error.stack}`;
      
      document.querySelector("#TreeComponent").appendChild(child);
    }
  }
  
  static getHeaderMarkupForNode(type, node) {
    // Fail silently in case no tags are defined.
    if (node.tags) {
      if (node.tags.status == "Complete")
        return `<sup>${type}</sup>`;
      else
        return `<sup><i>${node.tags.status} ${type}</i></sup>`;
    } else {
      return `<sup>${type}</sup>`;
    }
  }

  static getFooterMarkupForNode(tree, node) {
    // Fail silently in case no tags are defined.
    if (node.tags) {
      let tags = [];
    
      for (let tagSlug of Object.keys(node.tags)) {
        let tag = tree.Tags.filter(t => { return t.slug == tagSlug; })[0];
        
        if (tag) {
          let tagOption = tag.opts.filter(o => { return o.name == node.tags[tagSlug]; })[0];
          
          if (tagOption) {
            tags.push((tagOption.icon) ? `fa:${tagOption.icon}` : tagOption.name);
          }
        }
      }
  
      return `<sub>${tags.join(" / ")}</sub>`;
    } else {
      return "";
    }
  }

  static toggleTreeFlipped() {
    GlobalState.Preferences.isTreeFlipped = !GlobalState.Preferences.isTreeFlipped;
    TreeComponent.render(GlobalState.Components.cmEditor);
    m.redraw();
  }

  static filterTreeByStream(attackTree, streamSlug) {
    let filteredTree = {...attackTree};

    let stream = filteredTree.Streams.filter(s => { return s.slug === streamSlug; })[0];
    if (!stream) { return attackTree; }

    filteredTree.Artifacts = filteredTree.Artifacts.filter(a => {
      return stream.nodes.includes(a.slug);
    });
    
    filteredTree.Capabilities = filteredTree.Capabilities.filter(c => {
      return stream.nodes.includes(c.slug);
    });
    
    filteredTree.Impacts = filteredTree.Impacts.filter(i => {
      return stream.nodes.includes(i.slug);
    });

    filteredTree.Links = filteredTree.Links.filter(l => {
      return stream.nodes.includes(l.src) && stream.nodes.includes(l.dst);
    })
    
    return filteredTree;
  }
}

class FooterComponent {
  constructor(vnode) { }

  view() {

  }
}

class ModalsContainer {
  constructor(vnode) { }

  view() {
    return m("div[id='ModalsContainer']", [
      m(WelcomeModalComponent),
      m(AboutModalComponent)
    ]);
  }
}

class WelcomeModalComponent {
  constructor(vnode) { }

  view() {
    return m("div.ui.basic.tiny.modal[id='WelcomeModalComponent']", [
      m("div.ui.icon.header", [
        m("img[src='assets/images/walter-icon-white.svg']"),
        m("p", "Walter")]),
      m("div.content", [
        m("p", "Welcome to Walter, an attack path planning tool."),
        m("p", m("strong", "If this is your first time here, would you like to read the quick start guide?")),
        m("p", m("sub", "You can read it any time by clicking on the Help button."))]),
      m("div.actions", [
        m("div.ui.red.basic.cancel.inverted.button", [
          m("i.remove.icon"),
          m("span", "Skip")]),
        m("div.ui.green.ok.inverted.button", {onclick: e => { $("#AboutModalComponent.ui.modal").modal("show"); }}, [
          m("i.checkmark.icon"),
          m("span", "Yes")])]
      )]
    )
  }
}

class AboutModalComponent {
  constructor(vnode) { }

  view() {
    return m("div.ui.small.modal[id='AboutModalComponent']", [
      m("div.header", 
        m("p", "About Walter")),
      m("div.content", [
        m("p", [
          m("span", "Walter was written by "),
          m("a[href='http://jackson-t.ca']", "@Jackson_T"),
          m("span", " and benefits from the open-source works below:")]),
        m("table.ui.very.compact.small.table", [
          m("thead", 
            m("tr", [
              m("th", "Purpose"),
              m("th", "Name"),
              m("th", "Author"),
              m("th", "Licence")])),
          m("tbody", [
            m("tr", [
              m("td", "UI framework"),
              m("td", m("a[href='https://semantic-ui.com/']", "Semantic UI")),
              m("td", "Semantic UI LLC"),
              m("td", "MIT")]),
            m("tr", [
              m("td", "Component framework"),
              m("td", m("a[href='https://mithril.js.org/']", "Mithril")),
              m("td", "Leo Horie"),
              m("td", "MIT")]),
            m("tr", [
              m("td", "Text editor"),
              m("td", m("a[href='https://codemirror.net/']", "CodeMirror")),
              m("td", "Marjin Haverbeke"),
              m("td", "MIT")]),
            m("tr", [
              m("td", "Chart generator"),
              m("td", m("a[href='https://mermaidjs.github.io'", "mermaid")),
              m("td", "Knut Sveidqvist"),
              m("td", "MIT")]),
            m("tr", [
              m("td", "YAML parser"),
              m("td", m("a[href='https://github.com/nodeca/js-yaml'", "js-yaml")),
              m("td", "Vitaly Puzrin"),
              m("td", "MIT")]),
            m("tr", [
              m("td", "Pane splitter"),
              m("td", m("a[href='https://split.js.org'", "Split.js")),
              m("td", "Nathan Cahill"),
              m("td", "MIT")]),
            m("tr", [
              m("td", "Icon"),
              m("td", m("a[href='https://thenounproject.com/term/walter-white/54737/'", "Heisenberg")),
              m("td", "Emma Gilardi (Noun Project)"),
              m("td", "CC")])])]),
        m("p", [
          m("span", "The name is inspired by "),
          m("a[href='https://en.wikipedia.org/wiki/Walter_White_(Breaking_Bad)']", "Walter White"),
          m("span", ", who was always 10 steps ahead.")])]),
      m("div.actions", 
        m("div.ui.ok.button", "Close"))
      ]
    );
  }
}

function main() {
  m.mount(document.body, MainContainer);
}

if (window.addEventListener) window.addEventListener("load", main, false);
else if (window.attachEvent) window.attachEvent("onload", main);
else window.onload = main;